﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour {
	public string TargetScene;
	public void SwitchScene() {
		SceneManager.LoadScene(TargetScene);
	}

	// void Awake() {
	// 	Debug.Log(name + " Awake");
	// }

	// void OnEnable() {
	// 	Debug.Log(name + " On Enable");
	// }

	// void Start() {
	// 	Debug.Log(name + " Start");
	// }
}