﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DOTweenTest : MonoBehaviour {
	public Light PLight;
	int nbLoop = 0;

	// Use this for initialization
	void Start () {
		// ExampleNormal();
		// ExampleGeneric();  
		ExampleSequence();
	}
	
	void ExampleNormal() {
		transform
			.DOMoveX(3, 1)
			.SetLoops(-1, LoopType.Yoyo)
			.SetEase(Ease.OutBack)
			.OnStepComplete(() => {
				nbLoop++;
				if (nbLoop % 2 == 0) {
					PLight.DOColor(
						PLight.color == Color.cyan ? Color.red : Color.cyan,
						0.33f
					);
				}
			});
	}
	
	void ExampleSequence() {
		var sequence = DOTween.Sequence();
		sequence.Append(transform.DOMoveX(3, 1).SetEase(Ease.InBounce));
		sequence.Append(transform.DORotate(new Vector3(50, 50, 50), 1));
		sequence.SetLoops(-1, LoopType.Yoyo);
	}

	int exGeneric = 0;
	void ExampleGeneric() {
		DOTween.To(() => exGeneric, (int nb) => {
				exGeneric = nb;
				Debug.Log("Setter : nb = " + nb);
			},1000, 2
		);
	}
	
	// Update is called once per frame
	void Update () {

	}
}
